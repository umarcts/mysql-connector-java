Name: mysql-connector-java
Version: 5.1.35
Release: 1%{?dist}
Summary: Official JDBC driver for MySQL

Group: System Environment/Libraries
License: GPLv2 with exceptions
URL: http://dev.mysql.com/downloads/connector/j
Source0: http://dev.mysql.com/get/Downloads/Connector-J/%{name}-%{version}.tar.gz

%description
MySQL Connector is a JDBC -> MySQL network protocol driver.

%prep
%setup -q -n %{name}-%{version}

%install
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}
install -m 644 $RPM_BUILD_DIR/%{name}-%{version}/%{name}-%{version}-bin.jar \
    $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar

%files
%attr(0644,root,root) %{_javadir}/%{name}-%{version}.jar
