# MySQL Java Connector

The MySQL Java connector allows [Apache Sqoop](http://sqoop.apache.org) to
ingest data from a MySQL server to [Apache Hive](http://hive.apache.org) and
[Apache Hadoop](http://hadoop.apache.org).

## Building
Running `build.sh` will generate an RPM that contains the MySQL Java connector
jar. It does not build from source; rather, it copies the pre-built JAR in the
downloaded tarball. The MySQL Java Connector build process is absolutely
insane; it requires two versions of Java, and newer versions do not build
(although they run) on CentOS 6.
