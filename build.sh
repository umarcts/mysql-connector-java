#!/bin/sh

name="mysql-connector-java"
version="5.1.35"
tar_name="${name}-${version}.tar.gz"
src_dir="SOURCES"
src_name="${src_dir}/${tar_name}"

test -d ${src_dir} || mkdir ${src_dir}
test -f ${src_name} ||
	wget http://dev.mysql.com/get/Downloads/Connector-J/${tar_name} \
	-O ${src_name}

rpmbuild -ba \
	--define "_topdir $(pwd)" \
	mysql-connector-java.spec
	#--define "_sourcedir $(pwd)" \
	#--define "_builddir $(pwd)" \
	#--define "_srcrpmdir $(pwd)" \
	#--define "_rpmdir $(pwd)" \
	#mysql-connector-java.spec
